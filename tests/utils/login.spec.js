const webDriver = require('selenium-webdriver')
const assert = require('assert')
const getDriver = require('./getdriver')
const constants = require('./constants')
const { login} = require('./auth.util')
const By = webDriver.By
const until = webDriver.until


describe(
  'Verify Loginfunctionalities',
  () => {
    driver = getDriver()
    console.log("Hello")
    
    before(async () => {
        console.log("Hello1")
      driver.manage().window().maximize()
      await driver.get(constants.URL);
      console.log("Hello2")
      console.log("URL Entered is " +constants.URL)
      console.log("App URL " + constants.URL +" is opened successfully")
      var currenturl = await driver.getCurrentUrl()
      console.log("App URL is redirected to : " +currenturl)
     
    })
    
    
    

    it('Verify login with valid input', async () => {
      try {
        await login(driver)
        assert.ok(true)
        console.log("Login functionality verified successfully")
      } catch(e) {
        assert.fail(e)
      
      console.log("User is not able to login into app")

      }
      
    })

   
        

    after(() => {
    driver.quit();
    })
  


})
     
   
    